const mongoose = require('mongoose')

const defaultsConfig = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  bufferCommands: false,
  bufferMaxEntries: 0,
  keepAlive: true,
  useFindAndModify: false
}

let hasConnection = false

module.exports.setConnectionStatus = (status = false) => {
  return (hasConnection = status)
}

const replacerRegexStringify = (name, val) => {
  // // convert RegExp to string
  if (val && val.constructor === RegExp) {
    return val.toString()
  } else if (name === 'str') {
    return undefined
  }
  return val
}

const customLogger = (coll, op, doc, proj) => {
  process.stdout.write(`${coll}.${op}(${JSON.stringify(doc, replacerRegexStringify)}`)
  if (proj) {
    process.stdout.write(`,${JSON.stringify(proj, replacerRegexStringify)})\n`)
  } else {
    process.stdout.write(')\n')
  }
}

module.exports.createConnection = async (
  uri = process.env.MONGO_URI,
  config = defaultsConfig,
  verbose = false
) => {
  if (verbose) mongoose.set('debug', customLogger)
  if (hasConnection) return

  const options = { ...defaultsConfig, ...config }

  try {
    const connection = await mongoose.connect(
      uri,
      { ...options }
    )
    this.setConnectionStatus(true)

    return connection
  } catch (err) {
    console.log(err)
    throw new Error('Connection error')
  }
}

module.exports.startSession = () => {
  if (!hasConnection) return null
  return mongoose.startSession()
}

module.exports.closeConnection = async () => {
  await mongoose.connection.close()
}

mongoose.connection.on('connected', () => {
  console.log('Mongo connected')
  return true
})

mongoose.connection.on('disconnected', () => {
  console.log('Mongo has disconnected')
  this.setConnectionStatus(false)
  return true
})
