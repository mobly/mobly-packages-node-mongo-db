const {
  createConnection,
  closeConnection,
  setConnectionStatus,
  startSession
} = require('./connection')
const mongoose = require('mongoose')

describe('Test mongodb connection', () => {
  beforeEach(() => {
    jest.restoreAllMocks()
    setConnectionStatus(false)
  })

  it('set connection status:', async done => {
    const result = await setConnectionStatus()

    expect(result).toBe(false)

    done()
  })

  it('test params createConnection:', async done => {
    const error = new Error('Connection error')

    try {
      await createConnection()
    } catch (err) {
      expect(err).toEqual(error)
    }

    done()
  })

  it('create connection successfully:', async done => {
    const uri = 'mongodb://localhost:27017/test'
    const config = {}

    const resp = { ok: true }

    const connect = jest
      .spyOn(mongoose, 'connect')
      .mockImplementation(() => Promise.resolve(resp))

    const result = await createConnection(uri, config)

    expect(result).toEqual(resp)
    expect(connect).toHaveBeenCalled()
    done()
  })

  it('connected:', async done => {
    const uri = 'mongodb://localhost:27017/test'
    const config = {}

    const connect = jest.spyOn(mongoose, 'connect')

    setConnectionStatus(true)

    const result = await createConnection(uri, config)

    expect(connect).not.toHaveBeenCalled()
    expect(result).toEqual(undefined)
    done()
  })

  it('connection error:', async done => {
    const uri = 'mongodb://localhost:27017/test'
    const config = {}

    const error = new Error('Connection error')

    const connect = jest.spyOn(mongoose, 'connect').mockImplementation(() => {
      throw error
    })

    try {
      await createConnection(uri, config)
    } catch (err) {
      expect(err).toEqual(error)
      expect(connect).toHaveBeenCalled()
      done()
    }
  })

  it('start session with connection:', async done => {
    setConnectionStatus(true)

    const sessionSpy = jest.spyOn(mongoose, 'startSession').mockImplementation(() => {
      return true
    })

    const session = await startSession()

    expect(sessionSpy).toHaveBeenCalled()
    expect(session).toBe(true)
    done()
  })

  it('start session not connected:', async done => {
    setConnectionStatus(false)

    const session = await startSession()

    expect(session).toBe(null)
    done()
  })

  it('close connection:', done => {
    const mongo = mongoose.connection

    const close = jest.spyOn(mongo, 'close')

    closeConnection()

    expect(close).toHaveBeenCalled()
    done()
  })

  it('event connected:', async done => {
    mongoose.connection.readyState = 1 // set event connected

    const result = await mongoose.connection._events.connected()

    expect(result).toBe(true)

    done()
  })

  it('event disconnected:', async done => {
    // default mongoose.connection.readyState = 0 equals event disconnected
    const result = await mongoose.connection._events.disconnected()

    expect(result).toBe(true)

    done()
  })
})
