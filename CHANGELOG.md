# [1.5.0](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/compare/v1.4.0...v1.5.0) (2021-05-13)


### Bug Fixes

* **connection:** Awating connection to ble closed. ([d3fab9d](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/commits/d3fab9d))
* **test:** Fix unit tests ([d10cb6f](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/commits/d10cb6f))


### Features

* aguardando a conexao ser encerrada ([10fbdd6](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/commits/10fbdd6))

# [1.4.0](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/compare/v1.3.0...v1.4.0) (2020-08-25)


### Features

* Ajustando teste ([ccb9a51](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/commits/ccb9a51))
* colocando opcao de debug ([c31e8ce](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/commits/c31e8ce))

# [1.3.0](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/compare/v1.2.0...v1.3.0) (2020-04-14)


### Features

* **connection:** Adding new option to fulfil deprecation warning. ([c0dd624](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/commits/c0dd624))

# [1.2.0](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/compare/v1.1.2...v1.2.0) (2020-03-23)


### Features

*  Mudando defaults da conexao ([18ad5cb](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/commits/18ad5cb))

## [1.1.2](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/compare/v1.1.1...v1.1.2) (2020-02-21)


### Bug Fixes

* **readme:** Correcting word ([3c3d27d](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/commits/3c3d27d))

## [1.1.1](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/compare/v1.1.0...v1.1.1) (2020-01-02)


### Bug Fixes

* Colocando mongoose como peerDependency por conta do singleton diferente dentro do node_modules privado ([fa6974f](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/commits/fa6974f))

# [1.1.0](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/compare/v1.0.3...v1.1.0) (2019-12-27)


### Features

* Adicionando funcao startSession para criar sessoes e poder realizar transactions ([2f64598](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/commits/2f64598))

## [1.0.3](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/compare/v1.0.2...v1.0.3) (2019-10-31)


### Bug Fixes

* **readme:** Update readme with npm package installation ([e6f0478](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/commits/e6f0478))

## [1.0.2](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/compare/v1.0.1...v1.0.2) (2019-10-29)


### Bug Fixes

* **package:** Push package to npm flag ([11b51ae](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/commits/11b51ae))

## [1.0.1](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/compare/v1.0.0...v1.0.1) (2019-10-29)


### Bug Fixes

* **package:** Push to npm ([bb29126](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/commits/bb29126))

# 1.0.0 (2019-10-02)


### Features

* **main:** Implementing package ([e29404c](https://bitbucket.org/mobly/mobly-packages-node-mongo-db/commits/e29404c))
